<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Boat 
{
	/**
	 *
	 * @Assert\NotBlank
	 * @Assert\Length(min = 1)
	 *
     */
	protected $length;
	
	/**
	 *
	 * @Assert\NotBlank
     * @Assert\Length(min = 1)
     *
	 */
	protected $buttockAngle;	
	
	
	public function __construct($length, $buttockAngle)
	{
		$this->length = $length;
		$this->buttockAngle = $buttockAngle;
	}
	
	public function getLength()
	{
		return $this->length;
	}
	
	public function getButtockAngle()
	{
		return $this->buttockAngle;
	}
}
