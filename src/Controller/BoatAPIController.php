<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Boat;
use App\Services\BoatCalculations;

/**
 * @Route("/api/boat")
 */
class BoatAPIController extends AbstractController
{
	protected $defaultDisplacement = 500;
	
	protected $boatCalculatorPage     = 'boatcalc.page.twig';
	
	/**
     * @Route("/")
	 */	
	public function boatPowerCalculator()
	{
		return $this->render($this->boatCalculatorPage);
	}
	
	/**
     * @Route("/horse-power", methods={"GET"}, name="horsePower")
	 */
	public function horsePower(Request $request, ValidatorInterface $validator): Response
	{
		
		if (!$request->isXmlHttpRequest())
		{
            return $this->render($this->boatCalculatorPage);
        }
		
		$boat = new Boat(
						 $request->query->get('hl', null), # Boat Length
						 $request->query->get('ba', null)  # Buttock Angle 
						);
		
		$errors = $validator->validate($boat);
		
		if (count($errors) > 0)
		{
			
			# ~ For Debug purpose
			# $errors_string = (string) $errors;
			# return new Response($errors_string);
			
			return $this->json(['error'=>'Missing or Invalid Parameters']);
		}
		
		$boatCalc = new BoatCalculations($boat);
		
		$displacement      = $request->get('ds', $this->defaultDisplacement);
		
		$theoretical_speed = $boatCalc->calculateTheoreticalSpeed();
		
		$powers = [];
		
		for($knt=30; $knt > $theoretical_speed; $knt--)
		{
			$powers[$knt] = $boatCalc->horsePower($displacement, $knt);
		}
		
		$powers[$theoretical_speed] = $boatCalc->horsePower($displacement, $theoretical_speed);
		
		$results = [
					 'ds'     => $displacement,
					 'powers' => $powers
				   ]; 
				   
		return $this->json($results);
	}

}