<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
	/**
     * @Route("/", name="home")
	 */	
	public function boatPowerCalculator()
	{
		return $this->render('boatcalc.page.twig');
	}

}