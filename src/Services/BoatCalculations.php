<?php

namespace App\Services;

use App\Entity\Boat;

class BoatCalculations
{
	
	protected $boat;
	
	public function __construct(Boat $boat)
	{
		$this->boat = $boat;
	}
	
    public function  calculateSLRatio()
    {
        return ( $this->boat->getButtockAngle() * -0.2 ) + 2.9;
    }

	public function calculateTheoreticalSpeed()
	{
		return $this->calculateSLRatio() * sqrt($this->boat->getLength());
	}
	
	public function horsePower(int $displacement, $knots = null)
	{
		
		$boatLength = $this->boat->getLength();
		
		$SLRatio = $this->calculateSLRatio();
		
		$knots   = $knots ?? $SLRatio * sqrt($boatLength);
		
		$Cw      = 0.8 + (0.17 * $SLRatio);
		
		return ($displacement/1000) * pow($knots / ($Cw * sqrt($boatLength)), 3);
		
	}
}
